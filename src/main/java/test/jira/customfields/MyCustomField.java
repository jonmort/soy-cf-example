package test.jira.customfields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Map;

public class MyCustomField extends CalculatedCFType<String, String> {
    private static final Logger log = LoggerFactory.getLogger(MyCustomField.class);


    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);


        map.put("name", "foo");
        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

         FieldConfig fieldConfig = field.getRelevantConfig(issue);
         //add what you need to the map here

        return map;
    }

    @Override
    public String getStringFromSingularObject(String s) {
        return "value";
    }

    @Override
    public String getSingularObjectFromString(String s) throws FieldValidationException {
        return "value";
    }

    @Nullable
    @Override
    public String getValueFromIssue(CustomField customField, Issue issue) {
        return "value";
    }

}
